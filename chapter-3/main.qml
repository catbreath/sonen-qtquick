import QtQuick 2.2
import QtQuick.Controls 1.1
import QtMultimedia 5.0
import QtQuick.Particles 2.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Making Games with Qt Quick")
    color: "black"

    StackView {
        id: stackView
        initialItem: mainMenu

        Component {
            id: mainMenu

            Rectangle {
                color: "black"

                Column {
                    anchors.centerIn: parent
                    spacing: 20

                    Button {
                        text: "New Game"
                        onClicked: stackView.push(gameScreen)
                        width: 200
                        height: 50
                    }
                    Button {
                        text: "Exit"
                        onClicked: Qt.quit()
                        width: 200
                        height: 50
                    }
                }
            }
        }

        Component {
            id: gameScreen

            Item {
                Item {
                    id: gameController

                    property var coins: []
                    property int score: 0

                    // http://opengameart.org/content/spinning-gold-coin
                    Component.onCompleted: {
                        gameController.spawnCoin();

                        player.x = 1;
                        player.y = 1;
                    }

                    function spawnCoin() {
                        var coin = coinSpriteComponent.createObject(level);
                        coin.x = Math.random() * level.width;
                        coin.y = Math.random() * level.height;
                        coin.z = player.z - 1;
                        gameController.coins.push(coin);
                    }

                    Timer {
                        id: collisionTimer
                        running: true
                        repeat: true
                        interval: 10
                        onTriggered: {
                            for (var i = 0; i < gameController.coins.length; ) {
                                var coin = gameController.coins[i];
                                var playerCentre = Qt.point(player.width / 2, player.height / 2);
                                var pos = player.mapToItem(coin, playerCentre.x, playerCentre.y);
                                if (coin.contains(Qt.point(pos.x, pos.y))) {
                                    coin.destroy();
                                    gameController.coins.splice(i, 1);
                                    coinPickupAudio.play();
                                    ++gameController.score;
                                } else {
                                    ++i;
                                }
                            }
                        }
                    }

                    Timer {
                        running: true
                        repeat: true
                        interval: 1000
                        onTriggered: {
                            if (gameController.coins.length < 10) {
                                gameController.spawnCoin();
                            }
                        }
                    }

                    property Component coinSpriteComponent: AnimatedSprite {
                        source: "qrc:/images/coin.png"
                        width: frameWidth
                        height: frameHeight
                        frameCount: 9
                        frameDuration: 100
                        frameWidth: 32
                        frameHeight: 32
                        running: true
                    }
                }

                SoundEffect {
                    id: footstepsAudio
                    // https://www.freesound.org/people/DJ%20Chronos/sounds/46844/
                    source: "qrc:/audio/footsteps.wav"
                    loops: SoundEffect.Infinite
                }

                SoundEffect {
                    id: coinPickupAudio
                    // https://www.freesound.org/people/bradwesson/sounds/135936/
                    source: "qrc:/audio/coin.wav"
                }

                MouseArea {
                    anchors.fill: parent

                    function durationForDistance(p1, p2, pixelsPerSecond) {
                        var distanceAsPoint = Qt.point(p2.x - p1.x, p2.y - p1.y);
                        var distance = Math.sqrt(distanceAsPoint.x * distanceAsPoint.x + distanceAsPoint.y * distanceAsPoint.y);
                        return distance * 1000 / pixelsPerSecond;
                    }

                    onClicked: {
                        var targetPos = camera.mapToItem(level, mouseX, mouseY);
                        if (targetPos.x > 0 && targetPos.x < level.width && targetPos.y > 0 && targetPos.y < level.height) {
                            var playerCentre = Qt.point(player.x + player.width / 2, player.y + player.height / 2);
                            var animationDuration = durationForDistance(playerCentre, targetPos, 300);
                            xAnimation.duration = animationDuration;
                            yAnimation.duration = animationDuration;
                            player.x = targetPos.x - player.width / 2;
                            player.y = targetPos.y - player.height / 2;

                            if (animationDuration >= 221 && footstepsAudio.loopsRemaining == 0) {
                                footstepsAudio.loops = animationDuration / 221 + 1;
                                footstepsAudio.play();
                            }
                        }
                    }
                }

                Flickable {
                    id: camera
                    anchors.fill: parent
                    contentX: player.x + player.width / 2 - width / 2
                    contentY: player.y + player.height / 2 - height / 2
                    contentWidth: level.width
                    contentHeight: level.height
                    interactive: false

                    Image {
                        id: level
                        width: 1000
                        height: 1000
						// http://opengameart.org/sites/default/files/Castle_2.png
                        source: "qrc:/images/grass.png"
                        fillMode: Image.Tile

                        Image {
                            // http://opengameart.org/content/arena-game-sprites
                            id: player
                            source: "qrc:/images/player.png"
                            z: 1

                            Behavior on x {
                                NumberAnimation {
                                    id: xAnimation
                                }
                            }

                            Behavior on y {
                                NumberAnimation {
                                    id: yAnimation
                                }
                            }
                        }
                    }

                    ParticleSystem {
                        id: particles
                    }

                    ImageParticle {
                        anchors.fill: parent
                        system: particles
                        source: "qrc:/images/particle.png"
                        alpha: 0
                        colorVariation: 0.3
                    }

                    Emitter {
                        anchors.fill: parent
                        system: particles
                        emitRate: Math.sqrt(parent.width * parent.height) / 30
                        lifeSpan: 2000
                        size: 4
                        sizeVariation: 2

                        acceleration: AngleDirection {
                            angle: 90
                            angleVariation: 360
                            magnitude: 20
                        }
                        velocity: AngleDirection {
                            angle: -90
                            angleVariation: 360
                            magnitude: 10
                        }
                    }
                }

                Text {
                    id: scoreText
                    text: gameController.score
                    font.pixelSize: stackView.height * 0.1
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.margins: Math.min(stackView.width, stackView.height) * 0.05
                    color: "red"

                    SequentialAnimation {
                        loops: Animation.Infinite
                        running: true

                        NumberAnimation {
                            target: scoreText.anchors
                            property: "topMargin"
                            to: Math.min(stackView.width, stackView.height) * 0.025
                            duration: 500
                        }
                        NumberAnimation {
                            target: scoreText.anchors
                            property: "topMargin"
                            to: Math.min(stackView.width, stackView.height) * 0.05
                            duration: 500
                        }
                    }
                }
            }
        }
    }
}
