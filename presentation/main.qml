import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0

import Qt.labs.presentation 1.0

ApplicationWindow {
    id: window
    title: "Making Games With Qt Quick"
    width: 1280
    height: 720
    visible: true

    readonly property real fontSize: height * 0.05

    Presentation {
        anchors.fill: parent

        SlideCounter {
        }

        Clock {
        }

        Slide {
            title: "Introduction"
            delayPoints: true
            content: [
                "Mitch Curtis",
                "Software Engineer at The Qt Company",
                "Qt Quick (Enterprise) Controls",
                ""
            ]

            Calendar {
                id: calendar
                anchors.right: parent.right
                width: parent.fontSize * 8
                height: width
                visible: parent._pointCounter == parent.content.length - 1
            }
        }

        Slide {
            title: "Agenda"
            delayPoints: true
            content: [
                "What is Qt?",
                "What is Qt Quick?",
                "Why would I want to use either?",
                "Things offered by Qt Quick",
                "Making a small game and its UI with Qt Quick"
            ]
        }

        Slide {
            title: "What is Qt?"
            delayPoints: true
            content: [
                "qt.io/qt-framework says:\n\nQt is a cross-platform application and UI framework used for device creation and application development supporting deployment to over a dozen leading platforms.",
            ]
        }

        Slide {
            title: "What is Qt Quick?"
            delayPoints: true
            content: [
                "qt.io/qt-quick says:\n\nQt Quick is a modern user interface technology that separates the declarative UI design and the imperative programming logic. Instead of the traditional C++ APIs of Qt, the presentation layer of the application is written with a Qt-specific declarative language called QML.",
            ]
        }

        Slide {
            centeredText: "Why would I want to use either?"
        }

        Slide {
            title: "Qt"
            delayPoints: true
            content: [
                "Cross platform",
                "Nice API",
                "Well documented",
                "Well tested",
                "Mature",
                "Commercially & community driven",
            ]
        }

        Slide {
            title: "Companies using Qt"
            delayPoints: true

            GridLayout {
                width: parent.width * 0.8
                height: parent.height * 0.9
                rows: 2
                columns: 2
                anchors.centerIn: parent

                Repeater {
                    model: [
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-1.png",
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-2.png",
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-3.png",
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-4.png",
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-5.png",
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-6.png",
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-7.png",
                        "http://d3hp9ud7yvwzy0.cloudfront.net/wp-content/uploads/2014/06/company-8.png"
                    ]
                    delegate: Image {
                        source: modelData
                        fillMode: Image.PreserveAspectFit
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                    }
                }
            }
        }

        Slide {
            title: "Companies using Qt"
            content: ["European Space Agency, DreamWorks, Lucasfilm, Panasonic, Philips, Samsung, Siemens, Volvo, Walt Disney Animation Studios, Blizzard Entertainment, ..."]
        }

        Slide {
            title: "Software written with Qt"
            content: ["Autodesk Maya, Google Earth, KDE, Skype, Spotify, Ubuntu, VirtualBox, VLC, ..."]
        }

        Slide {
            title: "Qt Quick"
            delayPoints: true
            content: [
                "UI elements",
                "Super fast to code UIs with",
                "QML is pleasant to look at",
                "JavaScript for complex expressions/imperative scenarios",
                "C++ integration",
                "Powerful animations",
                "Particles and shaders"
            ]
        }

        Slide {
            title: "Qt Creator"
            delayPoints: true
            content: [
                "Super simple to use",
                "Powerful",
                "Nice refactoring options"
            ]
        }

        Slide {
            title: "Things offered by Qt Quick: Images"

            Column {
                anchors.centerIn: parent

                Image {
                    source: "http://www.google.com/images/srpr/logo11w.png"
                }
            }
        }

        Slide {
            title: "Text"

            Column {
                anchors.centerIn: parent
                spacing: fontSize

                Text {
                    text: "Lorem ipsum dolor sit amet"
                    font.pixelSize: fontSize * 2
                }
            }
        }

        Slide {
            id: offeringsSlide
            title: "Animations"

            SequentialAnimation {
                running: offeringsSlide.visible
                NumberAnimation {
                    target: loremText
                    property: "rotation"
                    from: 0
                    to: 360
                    duration: 1000
                }
                NumberAnimation {
                    target: loremText
                    property: "scale"
                    from: 1
                    to: 0
                    easing.type: Easing.InBounce
                    duration: 1000
                }
                NumberAnimation {
                    target: loremText
                    property: "scale"
                    from: 0
                    to: 1
                    easing.type: Easing.OutBounce
                    duration: 1000
                }
                SequentialAnimation {
                    loops: 3

                    NumberAnimation {
                        target: loremText
                        property: "opacity"
                        from: 1
                        to: 0.5
                        duration: 250
                    }
                    NumberAnimation {
                        target: loremText
                        property: "opacity"
                        from: 0.5
                        to: 1
                        duration: 250
                    }
                }
            }

            Column {
                anchors.centerIn: parent
                spacing: fontSize

                Text {
                    id: loremText
                    text: "Lorem ipsum dolor sit amet"
                    font.pixelSize: fontSize * 2
                }
            }
        }

        Slide {
            title: "Views"

            Flow {
                anchors.centerIn: parent
                spacing: fontSize

                ListView {
                    id: listView
                    width: fontSize * 4
                    height: fontSize * 4
                    clip: true

                    model: 12
                    delegate: Rectangle {
                        width: listView.width
                        height: 32
                        color: Qt.lighter("salmon", 1 + index / listView.count)

                        Text {
                            text: modelData
                            anchors.centerIn: parent
                        }
                    }
                }

                GridView {
                    id: gridView
                    width: fontSize * 2
                    height: fontSize * 2
                    cellWidth: gridView.width / 2
                    cellHeight: gridView.height / 2
                    clip: true

                    model: 12
                    delegate: Rectangle {
                        width: gridView.width / 2
                        height: gridView.height / 2
                        color: Qt.lighter("salmon", 1 + index / gridView.count)

                        Text {
                            text: modelData
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }
                }

                Item {
                    width: 180
                    height: 150

                    PathView {
                        id: pathView
                        model: 12
                        width: parent.width
                        height: parent.height
                        x: -30

                        delegate: Rectangle {
                            width: 32
                            height: 32
                            radius: width / 2
                            color: Qt.lighter("salmon", 1 + index / pathView.count)

                            Text {
                                text: modelData
                                anchors.centerIn: parent
                            }
                        }
                        path: Path {
                            startX: 120
                            startY: 100
                            PathQuad {
                                x: 120
                                y: 25
                                controlX: 260
                                controlY: 75
                            }
                            PathQuad {
                                x: 120
                                y: 100
                                controlX: -20
                                controlY: 75
                            }
                        }
                    }
                }
            }
        }

        Slide {
            title: "Shaders"

            ShaderExample {
                anchors.fill: parent

                Text {
                    text: "https://bitbucket.org/timday"
                    font.pixelSize: fontSize
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.bottom
                }
            }
        }

        Slide {
            title: "Making a small game and its UI with Qt Quick"
            delayPoints: true
            content: [
                "In terms of game code, we'll cover:",
                "Timers, images, animated sprites, a simple camera, dynamically created scene items, "
                + "particles, audio, simple collision detection, animations, mouse handling, coordinate mapping",
                "~220 lines of code"
            ]
        }

        Slide {
            title: "Chapter 1: Foundations"
            delayPoints: true

            content: [
                "Add main menu",
                "Add game screen"
            ]
        }

        Slide {
            title: "Chapter 2: The Game"
            delayPoints: true
            content: [
                "Add our player",
                "Give them movement",
                "Add a \"camera\"",
                "Give the ground a texture",
                "Add audio",
                "Add some pickups",
                "Add some environmental effects"
            ]

            Canvas {
                id: canvas
                width: window.width * 0.25
                height: window.height * 0.5
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                contextType: "2d"
                visible: parent._pointCounter === 2

                onContextChanged: {
                    if (context) {
                        context.lineWidth = 2;
                        context.strokeStyle = "red";
                    }
                }

                Rectangle {
                    anchors.fill: parent
                    color: "transparent"
                    border.color: "black"
                }

                MouseArea {
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    anchors.fill: parent
                    hoverEnabled: true

                    property int lastMouseX: 0
                    property int lastMouseY: 0
                    property int lastPressed: 0

                    onPressed: {
                        lastPressed = mouse.button;
                    }

                    onPositionChanged: {
                        if (pressed && lastPressed === Qt.LeftButton) {
                            canvas.context.beginPath();
                            canvas.context.moveTo(lastMouseX, lastMouseY);
                            canvas.context.lineTo(mouseX, mouseY);
                            canvas.context.stroke();
                            canvas.requestPaint();
                            canvas.context.closePath();
                        }
                        lastMouseX = mouseX;
                        lastMouseY = mouseY;
                    }

                    onClicked: {
                        if (mouse.button === Qt.RightButton) {
                            canvas.context.clearRect(0, 0, width, height);
                            canvas.requestPaint();
                            lastPressed = 0;
                        }
                    }
                }
            }
        }

        Slide {
            title: "Chapter 3: UI"
            delayPoints: true

            content: [
                "Add animated score counter"
            ]
        }
    }
}
