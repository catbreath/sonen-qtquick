import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Particles 2.0

Item {

  Item {
    id: main
    anchors.fill: parent

    CheckBox {
      id: liquify
      anchors.bottom: main.bottom
      anchors.left: main.left
      anchors.margins: 48
      text: "<b>Fluid</b>"
    }

    CheckBox {
      id: rain
      anchors.bottom: main.bottom
      anchors.right: main.right
      anchors.margins: 48
      text: "<b>Rain</b>"
    }

    Slider {
      id: rainrate
      orientation: Qt.Vertical
      minimumValue: 1
      maximumValue: 60
      stepSize: 1
      visible: rain.checked
      anchors.bottom: rain.top
      anchors.horizontalCenter: rain.horizontalCenter
    }

    ParticleSystem {
      anchors.fill: parent

      ImageParticle {
        source: "qrc:///particleresources/fuzzydot.png"
        entryEffect: ImageParticle.None
      }

      Emitter {
        enabled: rain.checked
        anchors.fill: parent
        emitRate: rainrate.value
        lifeSpan: 100
      }
    }

    CheckBox {
      id: bugs
      anchors.bottom: main.bottom
      anchors.right: rain.left
      anchors.margins: 48
      text: "<b>Bugs</b>"
    }

    Slider {
      id: bugcount
      orientation: Qt.Vertical
      minimumValue: 1
      maximumValue: 20
      stepSize: 1
      visible: bugs.checked
      anchors.bottom: bugs.top
      anchors.horizontalCenter: bugs.horizontalCenter
    }

    ParticleSystem {
      anchors.fill: parent

      ItemParticle {
        delegate: Rectangle {color: '#000000';width:5;height:5;radius:2}
        fade: false
      }

      Wander {
        pace: 300
        affectedParameter: Wander.Velocity
        xVariance: main.width
        yVariance: main.height
      }

      Emitter {
        enabled: bugs.checked
        emitRate: bugcount.value
        anchors.fill: parent
        maximumEmitted: bugcount.value
        lifeSpan: 5000
        lifeSpanVariation: 4000
      }
    }
  }

  ShaderEffectSource {
    id: base
    anchors.fill: parent
    live: true
    recursive: false
    sourceItem: main
    hideSource: true
  }

  ShaderEffectSource {
    id: buffer
    anchors.fill: parent
    live: false
    recursive: true
    sourceItem: buffer
    wrapMode: ShaderEffectSource.ClampToEdge

    Timer {
      id: ticker
      interval: 1000/30
      running: true
      repeat: true
      onTriggered: {
        buffer.scheduleUpdate();
        if (waves.p<0.5) waves.p=1.0-waves.p;else waves.p=0.5*Math.random(); // Avoid long-term bias
      }
    }

    ShaderEffect {
      id: waves
      anchors.fill: parent

      property variant src0: buffer
      property variant src1: base

      property bool running: liquify.checked

      property real dx: 1.0/buffer.width
      property real dy: 1.0/buffer.height

      property real c:  0.5   // Speed of light for waves
      property real dt: 0.5  // Timestep
      property real k:  0.0   // 0.0025 // Smoothing/viscosity  // Doesn't work great with 8bit quantization
      property real p:  0.5   // Random number to jitter rounding (avoids buffer quantization issues)

      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"

      fragmentShader: "
        uniform bool running;
        uniform highp float dx;
        uniform highp float dy;
        uniform highp float c;
        uniform highp float dt;
        uniform highp float k;
        uniform highp float p;
        varying highp vec2 coord;
        uniform sampler2D src0;
        uniform sampler2D src1;
        void main() {

          highp vec4 m=texture2D(src1,coord);

          // u,v nominally in range -0.5 to +0.5
      // Need to be able to represent small numbers more accurately
          // Buffer will contain a scale factor 0-1 in b channel
      // height 'u' is (r-0.5)*scale
      // velocity 'v' is (g-0.5)*scale

          if (!running || m.a>0.0) {
            gl_FragColor=vec4(0.5+0.5*m.a*(0.5+0.5*m.g),0.5,1.0,1.0);
          } else {

            highp vec3 ss=texture2D(src0,coord).rgb-vec3(0.5,0.5,0.0);
            highp vec2 s=ss.rg*ss.b;

            highp vec3 ssa0=texture2D(src0,coord+vec2(-dx,0.0)).rgb-vec3(0.5,0.5,0.0);
        highp vec3 ssa1=texture2D(src0,coord+vec2( dx,0.0)).rgb-vec3(0.5,0.5,0.0);
            highp vec3 ssa2=texture2D(src0,coord+vec2(0.0,-dy)).rgb-vec3(0.5,0.5,0.0);
            highp vec3 ssa3=texture2D(src0,coord+vec2(0.0, dy)).rgb-vec3(0.5,0.5,0.0);

        highp vec2 sa0=ssa0.rg*ssa0.b;
        highp vec2 sa1=ssa1.rg*ssa1.b;
        highp vec2 sa2=ssa2.rg*ssa2.b;
        highp vec2 sa3=ssa3.rg*ssa3.b;

            highp vec3 ssa4=texture2D(src0,coord+vec2(-dx,-dy)).rgb-vec3(0.5,0.5,0.0);
        highp vec3 ssa5=texture2D(src0,coord+vec2( dx,-dy)).rgb-vec3(0.5,0.5,0.0);
            highp vec3 ssa6=texture2D(src0,coord+vec2(-dx, dy)).rgb-vec3(0.5,0.5,0.0);
            highp vec3 ssa7=texture2D(src0,coord+vec2( dx, dy)).rgb-vec3(0.5,0.5,0.0);

        highp vec2 sa4=ssa4.rg*ssa4.b;
        highp vec2 sa5=ssa5.rg*ssa5.b;
        highp vec2 sa6=ssa6.rg*ssa6.b;
        highp vec2 sa7=ssa7.rg*ssa7.b;

            highp vec2 sa=sa0+sa1+sa2+sa3;
            highp vec2 so=sa4+sa5+sa6+sa7;

            // Smoothing
            s=(1.0-k)*s+k*(sa+so)/8.0;

            highp float dv=(dt*c*c)*(sa.r-4.0*s.r);

            highp float newv=s.g+dv;
            highp float newu=s.r+dt*newv;

        highp float scale=2.0*max(abs(newu),abs(newv));
        newu/=scale;
        newv/=scale;

            newu+=0.5;
            newv+=0.5;

            newu=floor(255.0*newu+p)/255.0;
            newv=floor(255.0*newv+p)/255.0;

            if (coord.x<dx || coord.x>1.0-dx || coord.y<dy || coord.y>1.0-dy) {
              newu=0.5;
              newv=0.5;
          scale=1.0;
            }

            gl_FragColor=vec4(newu,newv,scale,1.0);
          }
        }"
    }
  }

  // Shade the buffer height field
  ShaderEffectSource {
    id: recolor
    anchors.fill: parent
    live: true
    sourceItem: buffer
    hideSource: true

    ShaderEffect {
      anchors.fill: parent

      property real dx: 1.0/buffer.width
      property real dy: 1.0/buffer.height

      property variant src: parent.sourceItem

      vertexShader: "
        uniform highp mat4 qt_Matrix;
        attribute highp vec4 qt_Vertex;
        attribute highp vec2 qt_MultiTexCoord0;
        varying highp vec2 coord;
        void main() {
          coord = qt_MultiTexCoord0;
          gl_Position = qt_Matrix * qt_Vertex;
        }"

      fragmentShader: "
        uniform highp float dx;
        uniform highp float dy;
        varying highp vec2 coord;
        uniform sampler2D src;
        void main() {

          highp vec3 sa0=texture2D(src,coord+vec2(-dx,0.0)).rgb-vec3(0.5,0.5,0.0);
          highp vec3 sa1=texture2D(src,coord+vec2( dx,0.0)).rgb-vec3(0.5,0.5,0.0);
          highp vec3 sa2=texture2D(src,coord+vec2(0.0,-dy)).rgb-vec3(0.5,0.5,0.0);
          highp vec3 sa3=texture2D(src,coord+vec2(0.0, dy)).rgb-vec3(0.5,0.5,0.0);

      highp float ha0=sa0.r*sa0.b;
      highp float ha1=sa1.r*sa1.b;
      highp float ha2=sa2.r*sa2.b;
      highp float ha3=sa3.r*sa3.b;

          highp vec3 g=vec3(2.0*(ha0-ha1),2.0*(ha2-ha3),1.0);
          highp vec3 n=normalize(g);
          highp vec3 l=normalize(vec3(3.0,-2.0,2.0));
          highp float d=dot(n,l);

          gl_FragColor=vec4(d,d,d,1.0);
        }"
    }
  }
}
