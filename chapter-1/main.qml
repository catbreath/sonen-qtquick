import QtQuick 2.2
import QtQuick.Controls 1.1

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Making Games with Qt Quick")

    StackView {
        id: stackView
        initialItem: mainMenu

        Component {
            id: mainMenu

            Rectangle {
                color: "black"

                Column {
                    anchors.centerIn: parent
                    spacing: 20

                    Button {
                        text: "New Game"
                        onClicked: stackView.push(gameScreen)
                        width: 200
                        height: 50
                    }
                    Button {
                        text: "Exit"
                        onClicked: Qt.quit()
                        width: 200
                        height: 50
                    }
                }
            }
        }

        Component {
            id: gameScreen

            Rectangle {
                color: "darkgreen"
            }
        }
    }
}
